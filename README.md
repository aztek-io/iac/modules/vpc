# Module: vpc

Creates a VPC in AWS.

## Example Usage

How to create a VPC using this module:

```hcl
module "vpc" {
  source = "git@gitlab.com:aztek-io/iac/modules/vpc.git?ref=v0.4.0"
}

module "vpc_with_private_networks" {
  source             = "git@gitlab.com:aztek-io/iac/modules/vpc.git?ref=v0.4.0"
  private_networking = true
}
```

## Argument Reference

The following arguments are supported:

### Required Attributes

NONE

### Optional Attributes

* `ec2`                - (Optional|object()) Map containing instance size data for bastion. (Defaults to a m3.medium spot instance).
* `key_name`           - (Optional|string) The name of the key to use for the bastion (if there are private subnets). (Defaults to an empty string).
* `name`               - (Optional|string) The name of vpc. (Defaults to a random uuid).
* `private_networking` - (Optional|bool) Switch used to create private subnets. (Defaults to `false`).
* `tags`               - (Optional|map) tags to be associated with module resources. (Defaults to `{Name = var.name}`).
* `trusted_networks`   - (Optional|list(string)) CIDR blocks that the bastion can be accessed from. (Defaults to `0.0.0.0/0`).
* `vpc`                - (Optional|map(string)) CIDR blocks to be used in VPC creation. (Refer to `variables.tf` for default values).

## Attribute Reference

* `arn`             - The ARN of the vpc.
* `azs`             - The Availability Zones that the VPC id deployed to.
* `name`            - The name of the vpc.
* `nat_ips`         - The IPs associated with the NAT Gateways (if any).
* `nat_gateway_ids` - The ids associated with the NAT Gateways (if any).
* `public_subnets`  - The ids of the public subnets.
* `private_subnets` - The ids of the private subnets (if any).
* `sg_ssh`          - The id associated with the security group for ssh access.
* `sg_web`          - The id associated with the security group for web access.
* `sg_web_trusted`  - The id associated with the security group for web access from trusted networks.
