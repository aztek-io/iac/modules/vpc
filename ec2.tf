resource "aws_launch_configuration" "bastion" {
  count                       = var.private_networking ? 1 : 0
  name_prefix                 = local.bastion
  image_id                    = data.aws_ami.al2.id
  instance_type               = var.ec2["size"]
  key_name                    = var.key_name
  security_groups             = [aws_security_group.ssh_trusted.id]
  associate_public_ip_address = true
  user_data                   = file("${path.module}/bootstrap.sh")
  spot_price                  = var.ec2["spot_price"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "bastion" {
  count                     = var.private_networking ? 1 : 0
  name                      = local.bastion
  max_size                  = var.ec2["max_size"]
  min_size                  = var.ec2["min_size"]
  launch_configuration      = aws_launch_configuration.bastion[0].name
  vpc_zone_identifier       = aws_subnet.public[*].id
  health_check_grace_period = 0
  tags                      = local.asg_tags
}
