locals {
  name    = length(var.name) > 0 ? var.name : random_uuid.this.result
  bastion = "${local.name}-bastion"

  tags = merge(var.tags, local.default_tags)

  default_tags = {
    Name = local.name
  }

  asg_tags = [
    for tag_name in keys(local.tags): tomap(
      {
        "key" = tag_name,
        "value" = local.tags[tag_name],
        "propagate_at_launch" = "true"
      }
    )
  ]
}
