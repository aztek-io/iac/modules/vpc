########################################
### VPC ################################
########################################

resource "aws_vpc" "this" {
  cidr_block           = var.vpc["vpc_cidr"]
  enable_dns_hostnames = true

  tags = local.tags
}

########################################
### Internet Gateways ##################
########################################

resource "aws_internet_gateway" "this" {
  vpc_id = aws_vpc.this.id
  tags   = local.tags
}

########################################
### Subnets ############################
########################################

resource "aws_subnet" "public" {
  count             = length(data.aws_availability_zones.available.names)
  vpc_id            = aws_vpc.this.id
  cidr_block        = var.vpc["public_subnet_${count.index}"]
  availability_zone = data.aws_availability_zones.available.names[count.index]
  tags = merge(
    local.tags,
    {
      Public = true
      Name   = join("-", [local.name, "public-subnet", count.index])
    }
  )
}

resource "aws_subnet" "private" {
  count             = var.private_networking ? length(data.aws_availability_zones.available.names) : 0
  vpc_id            = aws_vpc.this.id
  cidr_block        = var.vpc["private_subnet_${count.index}"]
  availability_zone = data.aws_availability_zones.available.names[count.index]
  tags = merge(
    local.tags,
    {
      Public = false
      Name   = join("-", [local.name, "private-subnet", count.index])
    }
  )
}

########################################
### Route Tables #######################
########################################

# Public
####################

resource "aws_default_route_table" "this" {
  default_route_table_id = aws_vpc.this.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.this.id
  }

  tags = merge(
    local.tags,
    {
      Name = join("-", [local.name, "default-route-table"])
    }
  )
}

resource "aws_route_table" "private" {
  # Technically this can be done with one NAT instance, but in the interests of HA:
  count  = var.private_networking ? length(data.aws_availability_zones.available.names) : 0
  vpc_id = aws_vpc.this.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.private[count.index].id
  }

  tags = merge(
    local.tags,
    {
      Name = join("-", [local.name, element(data.aws_availability_zones.available.names, count.index)])
    }
  )

  lifecycle {
    ignore_changes = [
      route
    ]
  }
}

resource "aws_route_table_association" "public" {
  count          = length(data.aws_availability_zones.available.names)
  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_default_route_table.this.id
}

resource "aws_route_table_association" "private" {
  # Technically this can be done with one NAT instance, but in the interests of HA:
  count = var.private_networking ? length(data.aws_availability_zones.available.names) : 0

  subnet_id      = aws_subnet.private[count.index].id
  route_table_id = aws_route_table.private[count.index].id
}

########################################
### NAT Gateway ########################
########################################

resource "aws_eip" "nat_ip" {
  # Technically this can be done with one NAT instance, but in the interests of HA:
  count = var.private_networking ? length(data.aws_availability_zones.available.names) : 0
}

resource "aws_nat_gateway" "private" {
  # Technically this can be done with one NAT instance, but in the interests of HA:
  count = var.private_networking ? length(data.aws_availability_zones.available.names) : 0

  allocation_id = aws_eip.nat_ip[count.index].id
  subnet_id     = aws_subnet.public[count.index].id
  tags = merge(
    local.tags,
    {
      Name = join("-", [local.name, element(data.aws_availability_zones.available.names, count.index)])
    }
  )
}

########################################
### Security Groups ####################
########################################

resource "aws_security_group" "web_inbound" {
  vpc_id = aws_vpc.this.id

  tags = merge(
    local.tags,
    {
      Name = join("-", [local.name, "web-services"])
    }
  )

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }
}

resource "aws_security_group" "ssh_trusted" {
  vpc_id = aws_vpc.this.id
  tags = merge(
    local.tags,
    {
      Name = join("-", [local.name, "ssh-trusted"])
    }
  )

  ingress {
    cidr_blocks = var.trusted_networks
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }

  ingress {
    cidr_blocks = [var.vpc["vpc_cidr"]]
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }
}

resource "aws_security_group" "web_inbound_trusted" {
  vpc_id = aws_vpc.this.id
  tags = merge(
    local.tags,
    {
      Name = join("-", [local.name, "web-services-trusted"])
    }
  )

  ingress {
    cidr_blocks = concat(
      [var.vpc["vpc_cidr"]],
      var.trusted_networks
    )
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
  }

  ingress {
    cidr_blocks = concat(
      [var.vpc["vpc_cidr"]],
      var.trusted_networks
    )
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }
}
