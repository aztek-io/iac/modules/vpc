variable "ec2" {
  type = object(
    {
      size       = string
      spot_price = number
      min_size   = number
      max_size   = number
    }
  )

  default = {
    size       = "m3.medium"
    spot_price = 0.067
    min_size   = 1
    max_size   = 3
  }
}

variable "key_name" {
  default = ""
}

variable "name" {
  default = ""
}

variable "private_networking" {
  type    = bool
  default = false
}

variable "tags" {
  default = {}
}

variable "trusted_networks" {
  type = list(string)
  default = [
    "0.0.0.0/0"
  ]
}

variable "vpc" {
  type = map(string)
  default = {
    vpc_cidr = "100.64.0.0/16"

    public_subnet_0 = "100.64.0.0/24"
    public_subnet_1 = "100.64.1.0/24"
    public_subnet_2 = "100.64.2.0/24"
    public_subnet_3 = "100.64.3.0/24"
    public_subnet_4 = "100.64.4.0/24"
    public_subnet_5 = "100.64.5.0/24"
    public_subnet_6 = "100.64.6.0/24" # This is for future proofing in the event that us-east-1 grows to 7 azs

    private_subnet_0 = "100.64.100.0/24"
    private_subnet_1 = "100.64.101.0/24"
    private_subnet_2 = "100.64.102.0/24"
    private_subnet_3 = "100.64.103.0/24"
    private_subnet_4 = "100.64.104.0/24"
    private_subnet_5 = "100.64.105.0/24"
    private_subnet_6 = "100.64.106.0/24" # This is for future proofing in the event that us-east-1 grows to 7 azs
  }
}
