data "aws_availability_zones" "available" {}

data "aws_ami" "al2" {
  owners      = ["amazon"]
  most_recent = true
  name_regex  = "amzn2-ami-hvm-2.0.*x86_64-gp2"
}
