output "arn" {
  value = aws_vpc.this.arn
}

output "azs" {
  value = data.aws_availability_zones.available.names
}

output "public_subnets" {
  value = aws_subnet.public[*].id
}

output "private_subnets" {
  value = aws_subnet.private[*].id
}

output "name" {
  value = local.name
}

output "nat_ips" {
  value = aws_eip.nat_ip[*].public_ip
}

output "nat_gateway_ids" {
  value = aws_nat_gateway.private[*].id
}

output "sg_web" {
  value = aws_security_group.web_inbound.id
}

output "sg_ssh" {
  value = aws_security_group.ssh_trusted.id
}

output "sg_web_trusted" {
  value = aws_security_group.web_inbound_trusted.id
}
